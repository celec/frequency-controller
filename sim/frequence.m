vC = 0:1:10;
n = 1;
figure(1)
for c = vC
	f = fopen("freq.cir", "w"); % netlist
	fprintf(f, "* resonance selon varicap \n");
	fprintf(f, ".include opamp.mod\n");
	fprintf(f, "C2 Net-_C2-Pad1_ Net-_C1-Pad2_ 5.5f\n"); 
	fprintf(f, "L1 Net-_C2-Pad1_ Net-_L1-Pad2_ 18m\n");
	fprintf(f, "R1 Net-_C1-Pad1_ Net-_L1-Pad2_ 10\n") ;
	fprintf(f, "V1 Net-_C1-Pad1_ GND dc 0 ac 1\n");
	fprintf(f, "C1 Net-_C1-Pad1_ Net-_C1-Pad2_ 1p\n");
	fprintf(f, "R2 Net-_C1-Pad1_ Net-_C1-Pad2_ 10Meg\n"); 
	fprintf(f, "C3 Net-_C1-Pad1_ GND 20p\n") ; % capa pied 1
	fprintf(f, "C4 GND Net-_C1-Pad2_ 10p\n") ; % capa pied 2 
	fprintf(f, "R3 Net-_C1-Pad2_ GND 10k\n"); 
	fprintf(f, "V2 Net-_U1-Pad4_ GND dc 0 ac 0\n"); 
	fprintf(f, "R5 Vout GND 50\n"); 
	fprintf(f, "V3 Net-_U1-Pad7_ GND dc 5 ac 0\n"); 
	fprintf(f, "C5 Net-_C1-Pad2_ GND %fp\n", c); % varicap
	fprintf(f, "R4 Net-_R4-Pad1_ Vout 50\n");
	fprintf(f, "XU1 Net-_C1-Pad2_ Net-_R4-Pad1_ Vout opamp\n"); 
	fprintf(f, ".control \n");	
	fprintf(f, "ac lin 1024 15.996Meg 16Meg\n"); % Bode
	fprintf(f, "print vdb(Vout) vp(Vout) > t \n"); % plot
	fprintf(f, "quit \n") ; % 1=line, 2=freq, 3=dB, 4=ph
	fprintf(f, ".endc \n");
	fclose(f);
		
	system("ngspice freq.cir > /dev/null");
	system("cat t | grep ^[0-9] t > tt");
	system("rm t") ;
	load tt
	[a, b] = max(tt(:,3)); % max gain
	plot(tt(:,2) / 1e6, tt(:,3)), hold on;
	resonne(n) = tt(b, 2);
	n = n + 1;
end
xlabel("Fréquence (MHz)")
ylabel("Gain (dB)")

hold off;
figure(2)
plot(vC, resonne ./1e6)
xlabel("Varicap C (pF)")
ylabel("Fréquence oscillateur (MHz)")

pause()
