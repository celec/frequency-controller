#include <avr/io.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL

// sur timer 4
void pwm_setup(unsigned char a){
	TCCR4B = (1 << CS43) | (1 << CS41) | (1 << CS40); // prescale 1024
	TCCR4C = (1 << PWM4D) | (1 << COM4D1); // PWM, écoute sur OCR4D
	//TCCR4D |= (1 << WGM41) | (1 << WGM40); // 00 -> Fast PWM
	
	DDRD |= (1 << PORTD7); // D7/OC4D
	OCR4C = 0xff; // 0x3e80 : 1ms -> période du créneau
	OCR4D = 0xff / a; // 0x3e80/2 : 500us -> largeur du créneau (τ = 50%)
}

ISR(TIMER4_OVF_vect){
	//PORTF ^= 1 << PORTF1;
}

int main(void){
	unsigned char a = 10;

	MCUCR |= (1 << JTD);
	MCUCR |= (1 << JTD);
	DDRF |= 1 << PORTF1; //LED
	PORTF |= 1 << PORTF1;
	sei();
	USBCON = 0;

	pwm_setup(a);
	while(1){PORTF |= 1 << PORTF1;}
	return 0;
}
