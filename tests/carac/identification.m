% Point de fonctionnement à 30% 
% -> 34100 ticks où 1300 Hz

f = fopen("mesures.dat");
d = fscanf(f, "%x%x%d");

OVF = d(1:3:end);
r = d(2:3:end);
a = d(3:3:end);

F = 16e6; %Hz
p = 8; %prescale
err = ((2^16) * OVF + r) * p - F;

figure()
plot(a,r)
xlabel("Rapport cycle du PWM (%)")
ylabel("Écart en fréquence (Hz)")

pause()
