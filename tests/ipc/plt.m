F = 16e6; % F_CPU
p = 8; % prescaler
Nx = 30; %Nb OVF

f = fopen("res.bin");
d = fscanf(f, "%x");
err = ((2^16) * Nx + d) * p - F;

figure()
plot(-err, '*')
xlabel("Temps (s)")
ylabel('\Delta F (Hz)')

pause()
