// Lufa
#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include "serial.h"

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void transmit_data(uint8_t data){
	char buffer[2];
	buffer[0] = data; buffer[1] = 0;
	fputs(buffer, &USBSerialStream);
}

void write_char(unsigned char c){
	if ((c >> 4) > 9) transmit_data((c >> 4) - 10 + 'A'); else transmit_data((c >> 4) + '0');
	if ((c & 0x0f) > 9) transmit_data((c & 0x0f) - 10 + 'A'); else transmit_data((c & 0x0f) + '0');
}

void write_short(unsigned short s){
	write_char(s >> 8);
	write_char(s & 0xff);
}
