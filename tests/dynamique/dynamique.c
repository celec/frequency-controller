#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <util/delay.h>

#include "serial.h"
#include "ipc.h"
#include "pwm.h"
#include "communication.h"

volatile unsigned short res = 0;
volatile unsigned short ovf = 0;

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void init(void);
void mesure(float);
void commande(float);

ISR(TIMER1_CAPT_vect){
	res = ICR1;
	TCNT1 = 0;
}

ISR(TIMER1_OVF_vect){ovf++;}

int main (void){
	float duty = 0.2;
	char delai = 0;
	init();
	icp_setup();
	pwm_setup(duty);
	sei();
//	USBCON = 0;

	while (1){
		if (res != 0) {
			mesure(duty); 
			delai++;
			if (delai >= 15) {
				if (duty == 0.4) {duty = 0.2;}
				else {duty = 0.4;}
				delai = 0;
			}
			commande(duty);
		}
	}
	return 0;
}

void init(void){
	// LED
	MCUCR |= (1 << JTD);
	MCUCR |= (1 << JTD);
	DDRF |= (1 << PORTF1); // 1PPS
	DDRF |= (1 << PORTF6); // PWM ON
	PORTF &= ~(1 << PORTF1);
	PORTF |= (1 << PORTF6);

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
}

void mesure(float a){
	fputs("0x", &USBSerialStream); // Nb OVF
	write_short(ovf); //0x0018=24
	fputs("\t0x", &USBSerialStream); write_short(res); // erreur
	fprintf(&USBSerialStream, "\t%d\r\n", (int)(100.0 * a));		
	res = 0;
	ovf = 0;
	
	PORTF |= (1 << PORTF1); // LED à 1PPS
	_delay_ms(100);
	PORTF &= ~(1 << PORTF1);
	
	CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
	CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
	USB_USBTask();
}

void commande(float a){OCR4D = (int)((float)(OCR4C) * a);}
